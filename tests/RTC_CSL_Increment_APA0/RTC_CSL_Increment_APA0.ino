#include <RTC_CSL.h>
//#define Serial SerialUSB
#define Serial APA0

RTC_CSL rtc;


bool servoPower = true;
unsigned long cur, prev;

unsigned long curtime, rtctime, sysupdatetime;
void setup() {
  rtc.begin();

  cur = 0;
  prev = 0;
}
 
void loop() {
  curtime = micros();
  if(curtime - rtctime > 1000000){
    rtctime = curtime;
    cur = rtc.getCount();
    Serial.print("Current Count: ");  
    Serial.println(cur - prev);
    prev = cur; 
  }                              // Wait 1 second
}


