#include "wiring_private.h"

#define APA_RX_PAD SERCOM_RX_PAD_1
#define APA_TX_PAD UART_TX_PAD_0

#define APA0_TX_PIN  (17ul)            //A04
#define APA0_RX_PIN  (18ul)            //A05
#define APA0_RTS_PIN  (8ul)            //A06
#define APA0_CTS_PIN  (9ul)            //A07
#define APA0_PINMODE PIO_SERCOM_ALT
#define APA0_SERCOM (&sercom0)

#define BAUDRATE 115200
#define UARTCONFIG SERIAL_8N1

#define BOOT_DOUBLE_TAP_ADDRESS           (0x20007FFCul)
#define BOOT_DOUBLE_TAP_DATA              (*((volatile uint32_t *) BOOT_DOUBLE_TAP_ADDRESS))
#define DOUBLE_TAP_MAGIC                  (0x07738135)

Uart u0(APA0_SERCOM, APA0_RX_PIN, APA0_TX_PIN, APA_RX_PAD, APA_TX_PAD); 

char inbuffer[255];
uint8_t inbuflength = 0;

uint32_t curtime = 0;
uint32_t printtime = 0;

void setup() {
  
  pinPeripheral( APA0_RX_PIN, APA0_PINMODE); //PA09|D3
  pinPeripheral( APA0_TX_PIN, APA0_PINMODE); //PA08|D4

  APA0_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA0_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA0_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA0_SERCOM->enableUART();
}

void loop() {
  curtime = millis();
  // put your main code here, to run repeatedly:
  if(u0.available() > 0){
    u0.readBytesUntil('\n',inbuffer,255);
    if(inbuffer[0] == '!'){
      if(inbuffer[1] == 'r'){
        u0.println("Performing soft reset");
        BOOT_DOUBLE_TAP_DATA = DOUBLE_TAP_MAGIC; 
        NVIC_SystemReset();  
      }
    }
  }
  if(curtime - printtime > 1000){
    printtime = curtime;
    u0.println("TPing");
  }
}

void SERCOM0_Handler(){
  u0.IrqHandler();
}


SercomNumberStopBit extractNbStopBit(uint16_t config)
{
  switch(config & HARDSER_STOP_BIT_MASK)
  {
    case HARDSER_STOP_BIT_1:
    default:
      return SERCOM_STOP_BIT_1;

    case HARDSER_STOP_BIT_2:
      return SERCOM_STOP_BITS_2;
  }
}

SercomUartCharSize extractCharSize(uint16_t config)
{
  switch(config & HARDSER_DATA_MASK)
  {
    case HARDSER_DATA_5:
      return UART_CHAR_SIZE_5_BITS;

    case HARDSER_DATA_6:
      return UART_CHAR_SIZE_6_BITS;

    case HARDSER_DATA_7:
      return UART_CHAR_SIZE_7_BITS;

    case HARDSER_DATA_8:
    default:
      return UART_CHAR_SIZE_8_BITS;
  }
}

SercomParityMode extractParity(uint16_t config)
{
  switch(config & HARDSER_PARITY_MASK)
  {
    case HARDSER_PARITY_NONE:
    default:
      return SERCOM_NO_PARITY;

    case HARDSER_PARITY_EVEN:
      return SERCOM_EVEN_PARITY;

    case HARDSER_PARITY_ODD:
      return SERCOM_ODD_PARITY;
  }
}
