# SoftReset
A test of the SoftReset functionality.

If the chip receives a `!r` over APA0, it restarts using `NVIC_SystemReset();`. It also prompts the correct flags so that it stays in the bootloader rather than popping back out.

## The Soft Reset in Arduino
The Arduino soft reset uses a 1200bps signal over the USB line to initiate a reset. No such mechanism exists in on the UART port. There are some options to have the UART use auto-baud, but the information is not implicit like inn the metadata of the USB frame.

In an Arduino Uno, the RESET line is tied to DTR, allowing a reset to propagate over

### Viral Solution
All of the nodes communicate using APA, which uses a `{|}` packet format. These characters represent specific values of the DATA register in the serial port (regardless of protocol) which cause transitions in a port state machine. The solution is then to add another character which causes a transition to a separate state.
