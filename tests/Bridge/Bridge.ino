#include "wiring_private.h"

#define APA_RX_PAD SERCOM_RX_PAD_1
#define APA_TX_PAD UART_TX_PAD_0

#define APA0_TX_PIN  (17ul)            //A04
#define APA0_RX_PIN  (18ul)            //A05
#define APA0_RTS_PIN  (8ul)            //A06
#define APA0_CTS_PIN  (9ul)            //A07
#define APA0_PINMODE PIO_SERCOM_ALT
#define APA0_SERCOM (&sercom0)

#define APA2_TX_PIN  (22ul)            //A12
#define APA2_RX_PIN  (38ul)            //A13
#define APA2_RTS_PIN  (2ul)            //A14
#define APA2_CTS_PIN  (5ul)            //A15
#define APA2_PINMODE PIO_SERCOM_ALT
#define APA2_SERCOM (&sercom4)

#define BAUDRATE 115200
#define UARTCONFIG SERIAL_8N1

#define BOOT_DOUBLE_TAP_ADDRESS           (0x20007FFCul)
#define BOOT_DOUBLE_TAP_DATA              (*((volatile uint32_t *) BOOT_DOUBLE_TAP_ADDRESS))
#define DOUBLE_TAP_MAGIC                  (0x07738135)

#define FIRMWAREVERSION 2

Uart u0(APA0_SERCOM, APA0_RX_PIN, APA0_TX_PIN, APA_RX_PAD, APA_TX_PAD); 
Uart u2(APA2_SERCOM, APA2_RX_PIN, APA2_TX_PIN, APA_RX_PAD, APA_TX_PAD); 

char inbuffer[255];
uint8_t inbuflength = 0;
uint32_t curtime = 0;
uint32_t printtime = 0;
void setup() {
  // put your setup code here, to run once:
  pinPeripheral( APA0_RX_PIN, APA0_PINMODE); //PA09|D3
  pinPeripheral( APA0_TX_PIN, APA0_PINMODE); //PA08|D4

  APA0_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA0_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA0_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA0_SERCOM->enableUART();
  
  pinPeripheral( APA2_RX_PIN, APA2_PINMODE); //PA09|D3
  pinPeripheral( APA2_TX_PIN, APA2_PINMODE); //PA08|D4

  APA2_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA2_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA2_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA2_SERCOM->enableUART();
}

void loop() {
  curtime = millis();
  // put your main code here, to run repeatedly:
  if(u0.available() > 0){
    u0.readBytesUntil('\n',inbuffer,255);
    switch(inbuffer[0]){
      case '!':
        switch(inbuffer[1]){
          case 'R':
            u0.println("Performing soft reset");
            BOOT_DOUBLE_TAP_DATA = DOUBLE_TAP_MAGIC; 
            NVIC_SystemReset();  
            break;
          case 'v':
            u0.print("Version: ");
            u0.println(FIRMWAREVERSION);
            break;
        }
        break;
      case 't':
        switch(inbuffer[1]){
          case 'R':
            u2.write("!r");
            break;
          case 'i':
            u2.write("V#");
            break;
          case 'r':
            u2.write("we000ed00,4#");
            break;
        }
        break;
    }
  }
  if(u2.available() > 0){
    inbuflength = u2.readBytesUntil('\n',inbuffer,255);
    u0.print("(");
    u0.write(inbuffer,inbuflength);
    u0.println(")");
  }
  if(curtime - printtime > 1000){
    printtime = curtime;
    u0.println("Ping");
  }
}

void SERCOM0_Handler(){
  u0.IrqHandler();
}

void SERCOM4_Handler(){
  u2.IrqHandler();
}

SercomNumberStopBit extractNbStopBit(uint16_t config)
{
  switch(config & HARDSER_STOP_BIT_MASK)
  {
    case HARDSER_STOP_BIT_1:
    default:
      return SERCOM_STOP_BIT_1;

    case HARDSER_STOP_BIT_2:
      return SERCOM_STOP_BITS_2;
  }
}

SercomUartCharSize extractCharSize(uint16_t config)
{
  switch(config & HARDSER_DATA_MASK)
  {
    case HARDSER_DATA_5:
      return UART_CHAR_SIZE_5_BITS;

    case HARDSER_DATA_6:
      return UART_CHAR_SIZE_6_BITS;

    case HARDSER_DATA_7:
      return UART_CHAR_SIZE_7_BITS;

    case HARDSER_DATA_8:
    default:
      return UART_CHAR_SIZE_8_BITS;
  }
}

SercomParityMode extractParity(uint16_t config)
{
  switch(config & HARDSER_PARITY_MASK)
  {
    case HARDSER_PARITY_NONE:
    default:
      return SERCOM_NO_PARITY;

    case HARDSER_PARITY_EVEN:
      return SERCOM_EVEN_PARITY;

    case HARDSER_PARITY_ODD:
      return SERCOM_ODD_PARITY;
  }
}
