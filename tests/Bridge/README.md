# Bridge Test

The goal of the bridge test is to prototype program upload and communication.

## Sensor Topology
The topology used here involves connecting a bridge board `A` in between the USB and `B` such that high-level commands (such as "get the status of board B", "reset board B", or "erase board B and overwrite with your program")

    [USB] > 0[A]2 > 0[B]2

## High-Level Command Reference

commands can either be applied to Board A (apply to yourself) or Board B (through Board A)

Get status
reset
erase
write




## TODO

See BOSSA Analysis below for more info

  1. What is `0xe000ed00` and why can't `bossash > dump` read it?
  2. How does the checksum process work?
  3. can I write protect the bootloader and how does that change my workflow?
    * probably needs to be done in OpenOCD

## Programming workflow

  1. flush the serial port
  1. set binary mode
  1. send auto-baud
  1. set binary mode
  1. read info

## BOSSA Analysis
The arduino runs a BOSSA script to upload the code

    bossac -i -d --port=<PORT> -U true -i -e -w -v programtoflash.ino.bin -R

Reference:
  * **-i**: Display diagnostic information identifying the target device.
  * **-d**: Print verbose diagnostic messages for debug purposes.
  * **--port**: Use the serial port PORT to communicate with the device. By default this program will automatically scan all serial ports looking for a supported device.
  * **-U**: Enable automatic detection of the target's USB port if BOOL is false. Disable USB port autodetection if BOOL is true. (The former is default.)
  * **-e**: Erase the target's entire flash memory before performing any read or write operations.
  * **-w**: Write FILE to the target's flash memory. This operation can be expedited immensely if used in conjunction with the --erase option.
  * **-v**: Verify that FILE matches the contents of flash on the target, or vice-versa if you prefer.
  * **-R**: Reset the CPU after writing FILE to the target. This option is completely disregarded on unsupported devices.

(I assume everything happens in order, hence the repetition)

### Output from running the above command:

Attempting to upload the SoftReset code produces this (verbose output)

    Set binary mode
    Send auto-baud
    Set binary mode

**Set binary mode:** writes `N#` and reads 2 bytes. Sets Normal mode and target replies with `\n\r` to confirm that the mode has changed. Hence two calls- the second time is to confirm.

**Send auto-baud:** from `Samba.cpp` in the BOSSA source code.
```
// RS-232 auto-baud sequence
_port->put(0x80);
_port->get();
_port->put(0x80);
_port->get();
_port->put('#');
_port->read(cmd, 3);
```
This isn't necessary for the bridge

    readWord(addr=0)=0x20007ffc
    readWord(addr=0xe000ed00)=0x410cc601
    readWord(addr=0x41002018)=0x10010305


reading words. *~~(I get 0x00000000 when I try to read 0xe000ed00) using bossash, otherwise the output is the same~~ the bridge successfully reads it*

    version()=v2.0 [Arduino:XYZ] Jun 23 2017 17:51:36

The part where we check what this arduino is built to do (i.e. what its bootloader settings are)
Not going to worry about that here, since we have control over the bootloader software version (famous last words)

    chipId=0x10010005
    Connected at 115200 baud
    readWord(addr=0)=0x20007ffc
    readWord(addr=0xe000ed00)=0x410cc601
    readWord(addr=0x41002018)=0x10010305
    Atmel SMART device 0x10010005 found

Looks like the output of `bossash> info`, which is the result of reading the above addresses to get the board type.

    write(addr=0x20004000,size=0x34)
    writeWord(addr=0x20004030,value=0x10)
    writeWord(addr=0x20004020,value=0x20008000)

Something being written to SRAM?

    Device       : ATSAMD21G18A
    readWord(addr=0)=0x20007ffc
    readWord(addr=0xe000ed00)=0x410cc601
    readWord(addr=0x41002018)=0x10010305
    Chip ID      : 10010005
    version()=v2.0 [Arduino:XYZ] Jun 23 2017 17:51:36
    Version      : v2.0 [Arduino:XYZ] Jun 23 2017 17:51:36
    Address      : 8192
    Pages        : 3968
    Page Size    : 64 bytes
    Total Size   : 248KB
    Planes       : 1
    Lock Regions : 16
    Locked       : readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff
    readWord(addr=0x41004020)=0xffff

if you read the same address 16 times, it will not change.

    none
    readWord(addr=0x41004018)=0
    Security     : false
    Boot Flash   : true
    readWord(addr=0x40000834)=0x7000a
    BOD          : true
    readWord(addr=0x40000834)=0x7000a
    BOR          : true
    Arduino      : FAST_CHIP_ERASE
    Arduino      : FAST_MULTI_PAGE_WRITE
    Arduino      : CAN_CHECKSUM_MEMORY_BUFFER

Arduino options at the end

    Erase flash
    chipErase(addr=0x2000)
    done in 0.831 seconds

program starts at 0x2000 so erase the flash after that point (the bootloader is 8192 bytes in size)

According to `dump 0x804000 8`, this area isn't write protected. Marvelous.

    Write 10072 bytes to flash (158 pages)
    write(addr=0x20005000,size=0x1000)
    writeBuffer(scr_addr=0x20005000, dst_addr=0x2000,     size=0x1000)

    [============                  ] 40% (64/158 pages)write(addr=0x20005000,size=0x1000)
    writeBuffer(scr_addr=0x20005000, dst_addr=0x3000, size=0x1000)

    [========================      ] 81% (128/158 pages)write(addr=0x20005000,size=0x780)
    writeBuffer(scr_addr=0x20005000, dst_addr=0x4000, size=0x780)

    [==============================] 100% (158/158 pages)
    done in 1.487 seconds

program is written on 4096 byte increments. first it's written to SRAM (0x2000500), then transferred from SRAM to flash.

    Verify 10072 bytes of flash with checksum.
    checksumBuffer(start_addr=0x2000, size=0x1000) = 527
    checksumBuffer(start_addr=0x3000, size=0x1000) = 82c
    checksumBuffer(start_addr=0x4000, size=0x758) = 4be8

Then there is some checksum stuff, which I need to read up on.

    Verify successful
    done in 0.048 seconds
    CPU reset.
    readWord(addr=0)=0x20007ffc
    readWord(addr=0xe000ed00)=0x410cc601
    readWord(addr=0x41002018)=0x10010305
    writeWord(addr=0xe000ed0c,value=0x5fa0004)
