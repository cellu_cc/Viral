#include "SensorStickerV3.h"

//DEBUG Flag
//#define DEBUG_OUTPUT
//#define DEBUG_SPEED

//Debug "Serial"
//definition of Node ID
#define NODEID    (0x0)

#define Serial APA2

#if (NODEROLE < NODEROLE_BRIDGE)
  #if (NODEROLE == NODEROLE_ROOT)
    #define SENSORINTERVAL 20000 //microseconds, 50hz
  #else
    #define SENSORINTERVAL 10000 //microseconds, 100hz
  #endif
  // IMU Objects
  #if (SENSOR_SUITE == I2CSetup)
    Adafruit_FXAS21002C gyro = Adafruit_FXAS21002C(0x0021002C);
    Adafruit_FXOS8700 accelmag = Adafruit_FXOS8700(0x8700A, 0x8700B);
    
    // Mag calibration values are calculated via ahrs_calibration.
    // These values must be determined for each baord/environment.
    // See the image in this sketch folder for the values used
    // below.
    
    // Offsets applied to raw x/y/z mag values
    float mag_offsets[3]            = { 0.93F, -7.47F, -35.23F };
    
    // Soft iron error compensation matrix
    float mag_softiron_matrix[3][3] = { {  0.943,  0.011,  0.020 },
                                        {  0.022,  0.918, -0.008 },
                                        {  0.020, -0.008,  1.156 } };
    
    float mag_field_strength        = 50.23F;
    
    // Offsets applied to compensate for gyro zero-drift error for x/y/z
    float gyro_zero_offsets[3]      = { 0.0F, 0.0F, 0.0F };
    
    // Mahony is lighter weight as a filter and should be used
    // on slower systems
    Mahony filter;
    //Madgwick filter;
  #else
    
  #endif
  
  uint32_t sensortime;
  uint32_t ledtime;
  char output_buffer[255];
#endif


uint32_t curtime;

#ifdef DEBUG_OUTPUT
uint32_t portstatustime;
#define DebugSerial APA0
#endif

char apa3buffer[255];
volatile bool apa3bufferempty; 

// Callback for end-of-DMA-transfer
void dma_callback(struct dma_resource* const resource) {
  apa3bufferempty = true;
  SERCOM1->USART.INTENSET.bit.RXC = 1;
}
bool led_status = true;

////////////////////////////////////////////////////////////////////////////////////////
//
//  SETUP
//
//
////////////////////////////////////////////////////////////////////////////////////////
void setup() {
  //Serial.begin();
#if (NODEROLE < NODEROLE_BRIDGE)
  rtc.begin();
  pressureSensorSetup();
  IMUSetup();
  
  filter.begin(10);
  
  curtime = 0;
  sensortime = 0;
  ledtime = 0;
  pressureA.f = 0.0f;
  pressureB.f = 0.0f;
#endif
  DMASetup();
  apa3bufferempty = true;
}

////////////////////////////////////////////////////////////////////////////////////////
//
//  LOOP
//
//
////////////////////////////////////////////////////////////////////////////////////////

void loop() {
  curtime = micros();
#if (NODEROLE < NODEROLE_BRIDGE)
  if(curtime - sensortime > 20000){
    sensortime = curtime;
    updateIMU();
    updatePS();
    printStatus();
    //led_status = !led_status;
    //digitalWrite(PIN_LED_G, led_status);
    curtime = micros();
#ifdef DEBUG_SPEED
    DebugSerial.print("Sensor Time to Calc: ");
    DebugSerial.print(curtime-sensortime);
    DebugSerial.println("us");
#endif
  }
  if(curtime - ledtime > 1000000){
    ledtime = curtime;
    led_status = !led_status;
    digitalWrite(PIN_LED_R, led_status);
  }
#endif

#if (NODEROLE > NODEROLE_TIP)
#ifdef DEBUG_OUTPUT
  if(curtime - portstatustime > 1000000){
    portstatustime = curtime;
    DebugSerial.print("Buffer Status: ");
    DebugSerial.print(apa3bufferempty);
    DebugSerial.print(" ");
    DebugSerial.println(APA3state);
  }
#endif
  checkSerial();
#endif

}

#if (NODEROLE < NODEROLE_BRIDGE)
void IMUSetup(){
  if(!gyro.begin()){
    /* There was a problem detecting the gyro ... check your connections */
#ifdef DEBUG_OUTPUT
    DebugSerial.println("Ooops, no gyro detected ... Check your wiring!");
#endif
    while(1);
  }
  
  if(!accelmag.begin(ACCEL_RANGE_4G)){
#ifdef DEBUG_OUTPUT
    DebugSerial.println("Ooops, no FXOS8700 detected ... Check your wiring!");
#endif
    while(1);
  }
}

void updateIMU(){
  sensors_event_t gyro_event;
  sensors_event_t accel_event;
  sensors_event_t mag_event;

  gyro.getEvent(&gyro_event);
  accelmag.getEvent(&accel_event, &mag_event);
  
  // Apply mag offset compensation (base values in uTesla)
  float x = mag_event.magnetic.x - mag_offsets[0];
  float y = mag_event.magnetic.y - mag_offsets[1];
  float z = mag_event.magnetic.z - mag_offsets[2];

  // Apply mag soft iron error compensation
  float mx = x * mag_softiron_matrix[0][0] + y * mag_softiron_matrix[0][1] + z * mag_softiron_matrix[0][2];
  float my = x * mag_softiron_matrix[1][0] + y * mag_softiron_matrix[1][1] + z * mag_softiron_matrix[1][2];
  float mz = x * mag_softiron_matrix[2][0] + y * mag_softiron_matrix[2][1] + z * mag_softiron_matrix[2][2];

  // Apply gyro zero-rate error compensation
  float gx = gyro_event.gyro.x + gyro_zero_offsets[0];
  float gy = gyro_event.gyro.y + gyro_zero_offsets[1];
  float gz = gyro_event.gyro.z + gyro_zero_offsets[2];

  // The filter library expects gyro data in degrees/s, but adafruit sensor
  // uses rad/s so we need to convert them first (or adapt the filter lib
  // where they are being converted)
  gx *= 57.2958F;
  gy *= 57.2958F;
  gz *= 57.2958F;

  // Update the filter
  filter.update(gx, gy, gz,
                accel_event.acceleration.x, accel_event.acceleration.y, accel_event.acceleration.z,
                mx, my, mz);
  
  filter.getQuaternion(&(qw.f), &(qx.f), &(qy.f), &(qz.f));
}

void pressureSensorSetup(){
  //Initialize the Pressure Sensors
  ps_A.init();
  ps_B.init();
  delay(100);
  //turn on their default settings:
  //Pressure: 128 MPS (measurements per second), 1 SPM (Sample(s) per measurement)
  //Temperature: 8 MPS, 8 SPM
  ps_A.enableDefaultSettings();
  ps_B.enableDefaultSettings();
  
#ifdef DEBUG_OUTPUT
  int *c;
  DebugSerial.print("Calibration A: ");
  c = ps_A.getCalibration();
  for(int i = 0; i < 9; i++){
    DebugSerial.print(c[i], DEC);
    DebugSerial.print(" ");
  }
  DebugSerial.println();
  
  DebugSerial.print("Calibration B: ");
  c = ps_B.getCalibration();
  for(int i = 0; i < 9; i++){
    DebugSerial.print(c[i], DEC);
    DebugSerial.print(" ");
  }
  DebugSerial.println();
#endif
}

void updatePS(){
  pressureA.f = ps_A.getCalibratedPressure();
  pressureB.f = ps_B.getCalibratedPressure();
}

void printStatus(){
  if(apa3bufferempty){
    uint8_t data_length = snprintf(output_buffer, sizeof(output_buffer), "{I%02X#T%08X#Q%08X,%08X,%08X,%08X#P%08X,%08X#}", NODEID, rtc.getCount(), qw.s, qx.s, qy.s, qz.s, pressureA.s, pressureB.s);
    myDMA.setup_transfer_descriptor(
      output_buffer, // move data from here
      (void *)(&SERCOM2->USART.DATA.reg),   // to here
      data_length,                        // this many...
      DMA_BEAT_SIZE_BYTE,                   // bytes/hword/words
      true,                                 // increment source addr?
      false);                               // increment dest addr?
    apa3bufferempty = false;
    stat = myDMA.start_transfer_job(); // Go!
    printStatus(stat);
    //Serial.print(output_buffer);
  }
}
#endif

#if (NODEROLE > NODEROLE_TIP)
void checkSerial(){
  if(APA3state == PACKET_WAITING && apa3bufferempty){
    uint8_t data_length = APA3.readBytesUntil('}', apa3buffer, 255);
    apa3buffer[data_length++] = '}';
#if (NODEROLE == NODEROLE_BRIDGE)
    apa3buffer[data_length++] = '\n';
#endif
    APA3state = IDLE; 
    // Set up DMA transfer using the newly-filled buffer as source...
    myDMA.setup_transfer_descriptor(
      apa3buffer, // move data from here
      (void *)(&SERCOM2->USART.DATA.reg),   // to here
      data_length,                        // this many...
      DMA_BEAT_SIZE_BYTE,                   // bytes/hword/words
      true,                                 // increment source addr?
      false);                               // increment dest addr?

    apa3bufferempty = false;
    stat = myDMA.start_transfer_job(); // Go!
    printStatus(stat);
/*
#else
    Serial.print("Buffer: "); 
    Serial.println(apa3buffer);
    apa3bufferempty = true;*/
//#endif
  }
}
#endif


void DMASetup(){
#ifdef DEBUG_OUTPUT
  DebugSerial.println("Configuring");
#endif
  myDMA.configure_peripheraltrigger(SERCOM2_DMAC_ID_TX);
  myDMA.configure_triggeraction(DMA_TRIGGER_ACTON_BEAT);

#ifdef DEBUG_OUTPUT
  DebugSerial.print("Allocating...");
#endif
  stat = myDMA.allocate();
  printStatus(stat);

#ifdef DEBUG_OUTPUT
  DebugSerial.print("Adding descriptor...");
#endif
  stat = myDMA.add_descriptor();
  printStatus(stat);

#ifdef DEBUG_OUTPUT
  DebugSerial.println("Registering & enabling callback");
#endif
  myDMA.register_callback(dma_callback); // by default, called when xfer done
  myDMA.enable_callback(); // by default, for xfer done registers
}


void printStatus(status_code stat) {
#ifdef DEBUG_OUTPUT
  DebugSerial.print("Status ");
  switch (stat) {
    case STATUS_OK:
      DebugSerial.println("OK"); break;
    case STATUS_BUSY:
      DebugSerial.println("BUSY"); break;
    case STATUS_ERR_INVALID_ARG:
      DebugSerial.println("Invalid Arg."); break;
    default:
      DebugSerial.print("Unknown 0x"); DebugSerial.println(stat); break;
  }
#endif
}


