//I2C Library
#include <stdlib.h>
#include <Wire.h>
//RTC
#include <RTC_CSL.h>
//
// Sensors
//
//Pressure Sensor Library
#include <dps310.h>
//IMU/Sensor Fusion Library
#include <Adafruit_Sensor.h>
#include <Mahony.h>

#include <Adafruit_FXAS21002C.h>
#include <Adafruit_FXOS8700.h>

//DMA library
#include <Adafruit_ASFcore.h>
#include "status_codes.h"
#include <Adafruit_ZeroDMA.h>
#include "utility/dmac.h"
#include "utility/dma.h"

// DMA object
Adafruit_ZeroDMA myDMA;
status_code      stat; // For DMA status

#if(NODEROLE < NODEROLE_BRIDGE)
// Real-time counter
RTC_CSL rtc;


// Pressure Sensor Objects
DPS310 ps_A(0x77);
DPS310 ps_B(0x76);

//
// TYPEDEFS
//

// union for avoiding casting problems with sending a float as a hex string
typedef union _sendval{
  float f;
  uint32_t s;
} sendval;

sendval pressureA, pressureB;
sendval qw, qx, qy, qz;
#endif

