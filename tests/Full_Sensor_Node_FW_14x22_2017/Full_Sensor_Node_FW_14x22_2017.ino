#include "SensorStickerV3.h"

//DEBUG Flag
//#define DEBUG_OUTPUT
//#define DEBUG_SPEED

#define Serial APA2

#if (NODEROLE < NODEROLE_BRIDGE)
  #if (NODEROLE == NODEROLE_ROOT)
    #define SENSORINTERVAL 20000 //microseconds, 50hz
  #else
    #define SENSORINTERVAL 10000 //microseconds, 100hz
  #endif
  // IMU Objects
  #if (SENSOR_SUITE == I2CSETUP)
    Adafruit_FXAS21002C gyro = Adafruit_FXAS21002C(0x0021002C);
    Adafruit_FXOS8700 accelmag = Adafruit_FXOS8700(0x8700A, 0x8700B);
    
    // Mag calibration values are calculated via ahrs_calibration.
    // These values must be determined for each baord/environment.
    // See the image in this sketch folder for the values used
    // below.
    
    // Offsets applied to raw x/y/z mag values
    float mag_offsets[3]            = { 0.93F, -7.47F, -35.23F };
    
    // Soft iron error compensation matrix
    float mag_softiron_matrix[3][3] = { {  0.943,  0.011,  0.020 },
                                        {  0.022,  0.918, -0.008 },
                                        {  0.020, -0.008,  1.156 } };
    
    float mag_field_strength        = 50.23F;
    
    // Offsets applied to compensate for gyro zero-drift error for x/y/z
    float gyro_zero_offsets[3]      = { 0.0F, 0.0F, 0.0F };
    
    // Mahony is lighter weight as a filter and should be used
    // on slower systems
    Mahony filter;
    //Madgwick filter;
  #else
    SPIClass SPI1 (&sercom3, SPI_MISO, SPI_SCK, SPI_MOSI, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_2); 
    uint32_t val0 = 0;
    uint32_t val1 = 0;
    uint32_t val2 = 0;
    uint32_t val3 = 0;
  #endif
  
  uint32_t sensortime;
  uint32_t ledtime;
  char output_buffer[255];
#endif

#ifdef DEBUG_OUTPUT
  uint32_t portstatustime;
  #define DebugSerial APA0
#endif

uint32_t curtime;
char apa3buffer[255];
volatile bool apa3bufferempty; 

// Callback for end-of-DMA-transfer
void dma_callback(struct dma_resource* const resource) {
  apa3bufferempty = true;
  SERCOM1->USART.INTENSET.bit.RXC = 1;
  SERCOM4->USART.INTENSET.bit.RXC = 1;
  SERCOM0->USART.INTENSET.bit.RXC = 1;
}
bool led_status = true;

////////////////////////////////////////////////////////////////////////////////////////
//
//  SETUP
//
//
////////////////////////////////////////////////////////////////////////////////////////
void setup() {
  //Serial.begin();
#if (NODEROLE < NODEROLE_BRIDGE)
  rtc.begin();
  pressureSensorSetup();
  #if (SENSOR_SUITE == I2CSETUP)
    IMUSetup();
    filter.begin(10);
  #else
    pressureC.f = 0.0f;
    pressureD.f = 0.0f;
  #endif
  
  curtime = 0;
  sensortime = 0;
  ledtime = 0;
  pressureA.f = 0.0f;
  pressureB.f = 0.0f;
#endif
  DMASetup();
  apa3bufferempty = true;
}

////////////////////////////////////////////////////////////////////////////////////////
//
//  LOOP
//
//
////////////////////////////////////////////////////////////////////////////////////////

void loop() {
  curtime = micros();
#if (NODEROLE < NODEROLE_BRIDGE)
  if(curtime - sensortime > SENSORINTERVAL){
    sensortime = curtime;
    updateIMU();
    updatePS();
    printStatus();
    //led_status = !led_status;
    //digitalWrite(PIN_LED_G, led_status);
    curtime = micros();
#ifdef DEBUG_SPEED
    DebugSerial.print("Sensor Time to Calc: ");
    DebugSerial.print(curtime-sensortime);
    DebugSerial.println("us");
#endif
  }
  if(curtime - ledtime > 1000000){
    ledtime = curtime;
    led_status = !led_status;
    digitalWrite(PIN_LED_B, led_status);
  }
#endif

#if (NODEROLE > NODEROLE_TIP)
#ifdef DEBUG_OUTPUT
  if(curtime - portstatustime > 1000000){
    portstatustime = curtime;
    DebugSerial.print("Buffer Status: ");
    DebugSerial.print(apa3bufferempty);
    DebugSerial.print(" ");
    DebugSerial.println(APA3state);
  }
#endif
  checkSerial();
#endif

}

#if (NODEROLE < NODEROLE_BRIDGE)
  #if(SENSOR_SUITE == I2CSETUP)
void IMUSetup(){
  if(!gyro.begin()){
    /* There was a problem detecting the gyro ... check your connections */
#ifdef DEBUG_OUTPUT
    DebugSerial.println("Ooops, no gyro detected ... Check your wiring!");
#endif
    while(1);
  }
  
  if(!accelmag.begin(ACCEL_RANGE_4G)){
#ifdef DEBUG_OUTPUT
    DebugSerial.println("Ooops, no FXOS8700 detected ... Check your wiring!");
#endif
    while(1);
  }
}
  #endif

void updateIMU(){
  #if(SENSOR_SUITE == I2CSETUP)
  sensors_event_t gyro_event;
  sensors_event_t accel_event;
  sensors_event_t mag_event;

  gyro.getEvent(&gyro_event);
  accelmag.getEvent(&accel_event, &mag_event);
  
  // Apply mag offset compensation (base values in uTesla)
  float x = mag_event.magnetic.x - mag_offsets[0];
  float y = mag_event.magnetic.y - mag_offsets[1];
  float z = mag_event.magnetic.z - mag_offsets[2];

  // Apply mag soft iron error compensation
  float mx = x * mag_softiron_matrix[0][0] + y * mag_softiron_matrix[0][1] + z * mag_softiron_matrix[0][2];
  float my = x * mag_softiron_matrix[1][0] + y * mag_softiron_matrix[1][1] + z * mag_softiron_matrix[1][2];
  float mz = x * mag_softiron_matrix[2][0] + y * mag_softiron_matrix[2][1] + z * mag_softiron_matrix[2][2];

  // Apply gyro zero-rate error compensation
  float gx = gyro_event.gyro.x + gyro_zero_offsets[0];
  float gy = gyro_event.gyro.y + gyro_zero_offsets[1];
  float gz = gyro_event.gyro.z + gyro_zero_offsets[2];

  // The filter library expects gyro data in degrees/s, but adafruit sensor
  // uses rad/s so we need to convert them first (or adapt the filter lib
  // where they are being converted)
  gx *= 57.2958F;
  gy *= 57.2958F;
  gz *= 57.2958F;

  // Update the filter
  filter.update(gx, gy, gz,
                accel_event.acceleration.x, accel_event.acceleration.y, accel_event.acceleration.z,
                mx, my, mz);
  
  filter.getQuaternion(&(qw.f), &(qx.f), &(qy.f), &(qz.f));
  #endif
}


void pressureSensorSetup(){
#if (SENSOR_SUITE == I2CSETUP)
  //Initialize the Pressure Sensors
  ps_A.init();
  ps_B.init();
  delay(100);
  //turn on their default settings:
  //Pressure: 128 MPS (measurements per second), 1 SPM (Sample(s) per measurement)
  //Temperature: 8 MPS, 8 SPM
  ps_A.enableDefaultSettings();
  ps_B.enableDefaultSettings();
  
  #ifdef DEBUG_OUTPUT
    int *c;
    DebugSerial.print("Calibration A: ");
    c = ps_A.getCalibration();
    for(int i = 0; i < 9; i++){
      DebugSerial.print(c[i], DEC);
      DebugSerial.print(" ");
    }
    DebugSerial.println();
    
    DebugSerial.print("Calibration B: ");
    c = ps_B.getCalibration();
    for(int i = 0; i < 9; i++){
      DebugSerial.print(c[i], DEC);
      DebugSerial.print(" ");
    }
    DebugSerial.println();
  #endif
#else
  SPI1.begin();

  pinPeripheral(SPI_SCK, PIO_SERCOM);
  pinPeripheral(SPI_MOSI, PIO_SERCOM);
  pinPeripheral(SPI_MISO, PIO_SERCOM_ALT);

  pinMode(CS_0, OUTPUT);
  pinMode(CS_1, OUTPUT);
  pinMode(CS_2, OUTPUT);
  pinMode(CS_3, OUTPUT);

  digitalWrite(CS_0, HIGH);
  digitalWrite(CS_1, HIGH);
  digitalWrite(CS_2, HIGH);
  digitalWrite(CS_3, HIGH);

  uint8_t tmp = 0;

  //Reset Circuit

  writeRegister(0x0C, 0x09, CS_0);
  writeRegister(0x0C, 0x09, CS_1);
  writeRegister(0x0C, 0x09, CS_2);
  writeRegister(0x0C, 0x09, CS_3);

  delay(100);

  writeRegister(0x06, 0x70, CS_0);
  writeRegister(0x07, 0x33, CS_0);
  writeRegister(0x08, 0x07, CS_0);

  readCoeff(coeff0, CS_0);

  writeRegister(0x06, 0x70, CS_1);
  writeRegister(0x07, 0x33, CS_1);
  writeRegister(0x08, 0x07, CS_1);

  readCoeff(coeff1, CS_1);

  writeRegister(0x06, 0x70, CS_2);
  writeRegister(0x07, 0x33, CS_2);
  writeRegister(0x08, 0x07, CS_2);

  readCoeff(coeff2, CS_2);

  writeRegister(0x06, 0x70, CS_3);
  writeRegister(0x07, 0x33, CS_3);
  writeRegister(0x08, 0x07, CS_3);

  readCoeff(coeff3, CS_3);
#endif
}

void updatePS(){
#if (SENSOR_SUITE == I2CSETUP)
  //pressureA.f = ps_A.getCalibratedPressure();
  pressureB.f = ps_B.getCalibratedPressure();
#else
  pressureA.f = readCalibratedPressure(coeff0, CS_0);
  pressureB.f = readCalibratedPressure(coeff1, CS_1);
  pressureC.f = readCalibratedPressure(coeff2, CS_2);
  pressureD.f = readCalibratedPressure(coeff3, CS_3);
#endif
}

#if (SENSOR_SUITE == SPISETUP)
void writeRegister(uint8_t reg, uint8_t val, uint8_t cs){
  digitalWrite(cs, LOW);
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(reg);
  SPI1.transfer(val);
  SPI1.endTransaction();
  digitalWrite(cs, HIGH);
}

int toSignedInt(uint32_t value, uint8_t bitLength){
  int signedValue = value;
  if(value >> (bitLength-1))
    signedValue |= -1 << bitLength;
  return signedValue;
}


int readRawTemperature(uint8_t cs){
  uint32_t tval = 0;
  digitalWrite(cs, LOW);
  // put your main code here, to run repeatedly:
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x03 | (0x1 << 7));
  tval  = ((uint32_t)SPI1.transfer(0x00)) << 16;
  tval |= ((uint32_t)SPI1.transfer(0x00)) << 8;
  tval |=  (uint32_t)SPI1.transfer(0x00);
  SPI1.endTransaction();
  digitalWrite(cs, HIGH);

  return toSignedInt(tval, 24);
}

int readRawPressure(uint8_t cs){
  uint32_t tval = 0;
  digitalWrite(cs, LOW);
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x00 | (0x1 << 7));
  tval  = ((uint32_t)SPI1.transfer(0x00)) << 16;
  tval |= ((uint32_t)SPI1.transfer(0x00)) << 8;
  tval |=  (uint32_t)SPI1.transfer(0x00);
  SPI1.endTransaction();
  digitalWrite(cs, HIGH);

  return toSignedInt(tval, 24);
}

float readCalibratedTemp(int *coeff, uint8_t cs){
  int traw = readRawTemperature(cs);
  float scaledT = (float)traw*tscalefactor;
  return 0.5*coeff[0]+1.0*coeff[1]*scaledT;
}


float readCalibratedPressure(int *coeff, uint8_t cs){
  int traw = readRawTemperature(cs);
  int praw = readRawPressure(cs);
  float scaledT = (float)(toSignedInt(traw,24))*tscalefactor;
  float scaledP = (float)(toSignedInt(praw,24))*pscalefactor;
  return coeff[2] + scaledP*(coeff[3]+scaledP*(coeff[6]+scaledP*coeff[8])) + scaledT*coeff[4] + scaledT*scaledP*(coeff[5]+scaledP*coeff[7]);
}


void readCoeff(int *coeff, uint8_t cs){
  uint8_t calibration[18];
  uint32_t tcoeff[9];
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x00 | (0x1 << 7));

  digitalWrite(cs, LOW);
  // put your main code here, to run repeatedly:
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x10 | (0x1 << 7));
  for(uint8_t i = 0x10; i < 0x22; i++){
    calibration[i-0x10] = (uint8_t)(SPI1.transfer(0x00));
  }
  SPI1.endTransaction();
  digitalWrite(cs, HIGH);

  tcoeff[0] = ((calibration[0])<<4) | ((calibration[1]>>4)&0x0F);
  tcoeff[1] = ((0x0F&calibration[1])<<8)|(calibration[2]);
  tcoeff[2] = (calibration[3]<<12)|(calibration[4]<<4)|(calibration[5]>>4);
  tcoeff[3] = ((0x0F&calibration[5])<<16)|(calibration[6]<<8)|(calibration[7]);
  tcoeff[4] = (calibration[8]<<8)|(calibration[9]);
  tcoeff[5] = (calibration[10]<<8)|(calibration[11]);
  tcoeff[6] = (calibration[12]<<8)|(calibration[13]);
  tcoeff[7] = (calibration[14]<<8)|(calibration[15]);
  tcoeff[8] = (calibration[16]<<8)|(calibration[17]);

  int size[9] = {12,12,20,20,16,16,16,16,16};
  for(uint8_t i = 0; i < 9; i++){
    coeff[i] = toSignedInt(tcoeff[i],size[i]);
  }
}

#endif

void printStatus(){
  if(apa3bufferempty){
#if(SENSOR_SUITE == I2CSETUP)
    uint8_t data_length = snprintf(output_buffer, sizeof(output_buffer), "{I%02X#T%08X#Q%08X,%08X,%08X,%08X#P%08X,%08X#}", NODEID, rtc.getCount(), qw.s, qx.s, qy.s, qz.s, pressureA.s, pressureB.s);
#else
    uint8_t data_length = snprintf(output_buffer, sizeof(output_buffer), "{I%02X#T%08X#P%08X,%08X,%08X,%08X#}", NODEID, rtc.getCount(), pressureA.s, pressureB.s, pressureC.s, pressureD.s);
#endif
    myDMA.setup_transfer_descriptor(
      output_buffer, // move data from here
      (void *)(&SERCOM2->USART.DATA.reg),   // to here
      data_length,                        // this many...
      DMA_BEAT_SIZE_BYTE,                   // bytes/hword/words
      true,                                 // increment source addr?
      false);                               // increment dest addr?
    apa3bufferempty = false;
    stat = myDMA.start_transfer_job(); // Go!
    printStatus(stat);
    //Serial.print(output_buffer);
  }
}
#endif

#if (NODEROLE > NODEROLE_TIP)
void checkSerial(){
  if(APA3state == PACKET_WAITING && apa3bufferempty){
    uint8_t data_length = APA3.readBytesUntil('}', apa3buffer, 255);
    apa3buffer[data_length++] = '}';
#if (NODEROLE == NODEROLE_BRIDGE)
    apa3buffer[data_length++] = '\n';
#endif
    APA3state = IDLE; 
    // Set up DMA transfer using the newly-filled buffer as source...
    myDMA.setup_transfer_descriptor(
      apa3buffer, // move data from here
      (void *)(&SERCOM2->USART.DATA.reg),   // to here
      data_length,                        // this many...
      DMA_BEAT_SIZE_BYTE,                   // bytes/hword/words
      true,                                 // increment source addr?
      false);                               // increment dest addr?

    apa3bufferempty = false;
    stat = myDMA.start_transfer_job(); // Go!
    printStatus(stat);
/*
#else
    Serial.print("Buffer: "); 
    Serial.println(apa3buffer);
    apa3bufferempty = true;*/
//#endif
  }
#if (NODEROLE == NODEROLE_BRIDGE)
  if(APA2state == PACKET_WAITING && apa3bufferempty){
    uint8_t data_length = APA2.readBytesUntil('}', apa3buffer, 255);
    apa3buffer[data_length++] = '}';
    apa3buffer[data_length++] = '\n';
    APA2state = IDLE; 
    myDMA.setup_transfer_descriptor(
      apa3buffer, // move data from here
      (void *)(&SERCOM2->USART.DATA.reg),   // to here
      data_length,                        // this many...
      DMA_BEAT_SIZE_BYTE,                   // bytes/hword/words
      true,                                 // increment source addr?
      false);                               // increment dest addr?
    apa3bufferempty = false;
    stat = myDMA.start_transfer_job(); // Go!
    printStatus(stat);
  }
  if(APA0state == PACKET_WAITING && apa3bufferempty){
    uint8_t data_length = APA0.readBytesUntil('}', apa3buffer, 255);
    apa3buffer[data_length++] = '}';
    apa3buffer[data_length++] = '\n';
    APA0state = IDLE; 
    myDMA.setup_transfer_descriptor(
      apa3buffer, // move data from here
      (void *)(&SERCOM2->USART.DATA.reg),   // to here
      data_length,                        // this many...
      DMA_BEAT_SIZE_BYTE,                   // bytes/hword/words
      true,                                 // increment source addr?
      false);                               // increment dest addr?
    apa3bufferempty = false;
    stat = myDMA.start_transfer_job(); // Go!
    printStatus(stat);
  }
#endif
}
#endif


void DMASetup(){
#ifdef DEBUG_OUTPUT
  DebugSerial.println("Configuring");
#endif
  myDMA.configure_peripheraltrigger(SERCOM2_DMAC_ID_TX);
  myDMA.configure_triggeraction(DMA_TRIGGER_ACTON_BEAT);

#ifdef DEBUG_OUTPUT
  DebugSerial.print("Allocating...");
#endif
  stat = myDMA.allocate();
  printStatus(stat);

#ifdef DEBUG_OUTPUT
  DebugSerial.print("Adding descriptor...");
#endif
  stat = myDMA.add_descriptor();
  printStatus(stat);

#ifdef DEBUG_OUTPUT
  DebugSerial.println("Registering & enabling callback");
#endif
  myDMA.register_callback(dma_callback); // by default, called when xfer done
  myDMA.enable_callback(); // by default, for xfer done registers
}


void printStatus(status_code stat) {
#ifdef DEBUG_OUTPUT
  DebugSerial.print("Status ");
  switch (stat) {
    case STATUS_OK:
      DebugSerial.println("OK"); break;
    case STATUS_BUSY:
      DebugSerial.println("BUSY"); break;
    case STATUS_ERR_INVALID_ARG:
      DebugSerial.println("Invalid Arg."); break;
    default:
      DebugSerial.print("Unknown 0x"); DebugSerial.println(stat); break;
  }
#endif
}


