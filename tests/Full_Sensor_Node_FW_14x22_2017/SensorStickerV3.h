#include <stdlib.h>
//RTC
#include <RTC_CSL.h>

//Define sensor setup options
#define I2CSETUP (0u)
#define SPISETUP (1u)

//set the sensor setup variable 
#define SENSOR_SUITE SPISETUP

//definition of Node ID
#define NODEID    (0x15)

//DMA library
#include <Adafruit_ASFcore.h>
#include "status_codes.h"
#include <Adafruit_ZeroDMA.h>
#include "utility/dmac.h"
#include "utility/dma.h"

// DMA object
Adafruit_ZeroDMA myDMA;
status_code      stat; // For DMA status

//
// TYPEDEFS
//

// union for avoiding casting problems with sending a float as a hex string
typedef union _sendval{
  float f;
  uint32_t s;
} sendval;

//
// Sensors
//
#if (NODEROLE < NODEROLE_BRIDGE)
  // Real-time counter
  RTC_CSL rtc;
  
  #if (SENSOR_SUITE == I2CSETUP)
    //I2C Library
    #include <Wire.h>
    //Pressure Sensor Library
    #include <dps310.h>
    //IMU/Sensor Fusion Library
    #include <Adafruit_Sensor.h>
    #include <Mahony.h>
    
    #include <Adafruit_FXAS21002C.h>
    #include <Adafruit_FXOS8700.h>
    // Pressure Sensor Objects
    DPS310 ps_A(0x77);
    DPS310 ps_B(0x76);
    
    sendval pressureA, pressureB;
    sendval qw, qx, qy, qz;
  #else 
    #include <SPI.h>
    #include "wiring_private.h"
    #define SPI_SCK  21
    #define SPI_MISO  6
    #define SPI_MOSI 20
    
    #define CS_0 7   //PA21 - Pin 30 on the D21G
    #define CS_1 16  //PB09 - Pin 8 on the D21G
    #define CS_2 24  //PB11 - Pin 20 on the D21G
    #define CS_3 14  //PA02 - Pin 3 on the D21G
    
    const float pscalefactor = 1.0/524288.0; //1 OSR
    const float tscalefactor = 1.0/7864320.0;//8 OSR
    
    int coeff0[9];
    int coeff1[9];
    int coeff2[9];
    int coeff3[9];
    
    sendval pressureA, pressureB, pressureC, pressureD;
  #endif
#endif



