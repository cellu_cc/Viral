/*
  Copyright (c) 2015 Arduino LLC.  All right reserved.
  Copyright (c) 2015 Atmel Corporation/Thibaut VIARD.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _BOARD_DEFINITIONS_H_
#define _BOARD_DEFINITIONS_H_

#include <stdbool.h>
/*
 * If BOOT_DOUBLE_TAP_ADDRESS is defined the bootloader is started by
 * quickly tapping two times on the reset button.
 * BOOT_DOUBLE_TAP_ADDRESS must point to a free SRAM cell that must not
 * be touched from the loaded application.
 */
#define BOOT_DOUBLE_TAP_ADDRESS           (0x20007FFCul)
#define BOOT_DOUBLE_TAP_DATA              (*((volatile uint32_t *) BOOT_DOUBLE_TAP_ADDRESS))

/*
 * If BOOT_LOAD_PIN is defined the bootloader is started if the selected
 * pin is tied LOW.
 */
//#define BOOT_LOAD_PIN                     PIN_PA21 // Pin 7
//#define BOOT_LOAD_PIN                     PIN_PA15 // Pin 5
#define BOOT_PIN_MASK                     (1U << (BOOT_LOAD_PIN & 0x1f))

#define CPU_FREQUENCY                     (48000000ul)

//
// APA0 Port
//
#define APA0_USART_MODULE                 SERCOM0
#define APA0_USART_BUS_CLOCK_INDEX        PM_APBCMASK_SERCOM0
#define APA0_USART_PER_CLOCK_INDEX        SERCOM0_GCLK_ID_CORE
#define APA0_USART_PAD_SETTINGS           UART_RX_PAD1_TX_PAD0
#define APA0_USART_PAD3                   PINMUX_UNUSED//PINMUX_PA11C_SERCOM0_PAD3
#define APA0_USART_PAD2                   PINMUX_UNUSED//PINMUX_PA10C_SERCOM0_PAD2
#define APA0_USART_PAD1                   PINMUX_PA05D_SERCOM0_PAD1
#define APA0_USART_PAD0                   PINMUX_PA04D_SERCOM0_PAD0

//
// APA2 Port
//
#define APA2_USART_MODULE                 SERCOM4
#define APA2_USART_BUS_CLOCK_INDEX        PM_APBCMASK_SERCOM4
#define APA2_USART_PER_CLOCK_INDEX        SERCOM4_GCLK_ID_CORE
#define APA2_USART_PAD_SETTINGS           UART_RX_PAD1_TX_PAD0
#define APA2_USART_PAD3                   PINMUX_UNUSED//PINMUX_PA11C_SERCOM0_PAD3
#define APA2_USART_PAD2                   PINMUX_UNUSED//PINMUX_PA10C_SERCOM0_PAD2
#define APA2_USART_PAD1                   PINMUX_PA13D_SERCOM4_PAD1
#define APA2_USART_PAD0                   PINMUX_PA12D_SERCOM4_PAD0

/* Frequency of the board main oscillator */
#define VARIANT_MAINOSC	                  (32768ul)

/* Master clock frequency */
#define VARIANT_MCK			                  CPU_FREQUENCY

#define NVM_SW_CALIB_DFLL48M_COARSE_VAL   (58)
#define NVM_SW_CALIB_DFLL48M_FINE_VAL     (64)

/*
 * LEDs definitions
 */
#define BOARD_LED_PORT                    (1)
#define BOARD_LED_PIN                     (23)

#define BOARD_LEDRX_PORT                  (1)
#define BOARD_LEDRX_PIN                   (2)

#define BOARD_LEDTX_PORT                  (1)
#define BOARD_LEDTX_PIN                   (22)

extern volatile bool serialPortAPA0;

#endif // _BOARD_DEFINITIONS_H_
