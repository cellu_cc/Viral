#target extended-remote /dev/cu.usbmodem7BB157B1
#target extended-remote /dev/cu.usbmodem7BAD7081

define startup
  set mem inaccessible-by-default off
end

define upload1
  monitor swdp_scan
  attach 1
  file build/samd21_sam_ba.elf
  load
  monitor swdp_scan
  attach 1
end

define upload2
  shell make clean
  shell make
  upload1
end

define upload3
  upload2
  set {uint32_t}0x20007FFC=0x07738135
end
