#ifndef VIRAL_AUTOMATON_H
#define VIRAL_AUTOMATON_H

#include <stdbool.h>
#include "sam_ba_serial.h"
#include "sam_ba_monitor.h"
#include "sam_ba_programmer.h"
#include "board_definitions.h"
#include "NVMDefinition.h"
//
// DEFINES
//

#define BUFFER_SIZE 32

//
// TYPES
//

typedef enum _viral_state{
  POLLING,
  WAITING_FOR_START,
  RECEIVING,
  TRANSMITTING,
  APPLICATION
}viral_state;


typedef struct _neighbor_state{
  SerialPort *port;
  uint8_t *output_buffer;
  uint8_t *out_location;
  uint8_t outbufferlen;
  uint8_t input_val;
  uint32_t current_number;
  uint8_t command;
  uint32_t timeout;
  uint32_t version;
} neighbor_state;


//
//VARIABLES
//
extern volatile viral_state automaton_state;

neighbor_state *neighborA, *neighborB, *neighborC, *neighborD;
neighbor_state *targetNeighbor;

uint32_t self_version;

//
// FUNCTIONS
//

//define the neighbors
void initialize_automaton();

//initialize neighbor struct
neighbor_state * init_neighbor(SerialPort *_port);

//poll the input/output buffers to check if anything has been sent/received yet.
bool automaton_polling_state();
bool automaton_transmitting_state();
bool automaton_receiving_state();

//check on the neighbor
bool update_neighbor(neighbor_state *neighbor);

uint32_t automaton_software_version();

#endif
