#ifndef NVMDEFINITION_H
#define NVMDEFINITION_H
//
// NVM (flash) information
//

#include <sam.h>

#define NVM_MEMORY        ((volatile uint16_t *)(0x00000000U))

#define NVM_CTRLA_REG             (0x41004000)
#define NVM_INT_STATUS_REG        (NVM_CTRLA_REG + 0x14)
#define NVM_STATUS_REG            (NVM_CTRLA_REG + 0x18)

// NVM ready bit mask
#define NVM_INT_STATUS_READY_MASK (0x1)
//#define NVMCTRL_STATUS_MASK       (0xFFEBu)

// NVM input register to some of the CMDEX commands.
#define NVM_ADDR_REG              (NVM_CTRLA_REG+0x1c)

// CMDEX field should be 0xA5 to allow execution of any command.
#define CMDEX_KEY                 (0xa500u)

// List of NVM Commands.//as per datasheet prefix CMDEX
#define CMD_ERASE_ROW             (CMDEX_KEY | 0x0002u)
#define CMD_WRITE_PAGE            (CMDEX_KEY | 0x0004u)

//NVM defines

//Pages and rows are defined by the controller
#define BYTESPERPAGE              (64)      //number of bytes per page
#define FLASHPAGESPERROW          (4)       //rows per page

//Total flash sizes, in number of pages
#define NVMPAGES                  (4096)    //number of pages in NVM
#define BOOTLOADERPAGES           (256)  //size of the bootloader in bytes
#define EEPROMPAGES               (4)      //size of the EEPROM in pages

#endif
