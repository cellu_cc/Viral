
#ifndef _PROGRAMMER_SAM_BA_H_
#define _PROGRAMMER_SAM_BA_H_

#include <sam.h>
#include <string.h>
#include "sam_ba_serial.h"
#include "board_driver_serial.h"
#include "board_driver_led.h"
#include "sam_ba_cdc.h"
#include "board_definitions.h"
#include "NVMDefinition.h"
//
// Target identification memory locations
// (assumes a SAMD series microcontroller)
//
#define TARGET_CPUID_ADR (0xe000ed00)
#define TARGET_CPUID_MSK (0x0000fff0)
#define TARGET_CPUID_VAL (0xc600)

#define TARGET_DEVID_ADR (0x41002018)

#define BUFFER_ADDRESS   (0x20005000)

#define FLASH_SIZE (0x1000)

uint8_t dataBuffer[(int)FLASH_PAGE_SIZE*2];
uint8_t cmdBuffer[PKTLEN_128+5];
uint8_t cmdLen;

//
// PROGRAMMER METHODS
//
void sam_ba_program(SerialPort *serPort);

void programmer_set_normal_mode(SerialPort *serPort);
bool programmer_verify_board(SerialPort *serPort);

uint16_t programmer_checksum_target(SerialPort *serPort, uint32_t start_address, uint32_t size);
uint16_t programmer_checksum_self(uint32_t start_address, uint32_t size);

void programmer_write_word(SerialPort *serPort, uint32_t addr, uint32_t val);
void programmer_write(SerialPort *serPort, uint32_t src_addr, uint32_t size);
void programmer_write_buffer(SerialPort *serPort, uint32_t scr_addr, uint32_t dst_addr, uint32_t size);

bool programmer_write_with_polling(SerialPort *serPort, uint8_t *buf, uint8_t send_length, uint8_t response_length, uint32_t timeout);

void programmer_get_data_and_wait(SerialPort *serPort, uint8_t *buf, uint32_t size);

void programmer_reset_target(SerialPort *serPort);

bool read_block_NVM_to_buffer(const uint32_t source_address, uint8_t * tbuffer, uint32_t size);
void programmer_update_vrow(SerialPort *serPort);

uint32_t programmer_read_word(SerialPort *serPort, uint32_t addr);
 uint8_t programmer_read_byte(SerialPort *serPort, uint32_t addr);

void programmer_erase_target(SerialPort *serPort);
void programmer_update_vrow(SerialPort *serPort);

#endif
