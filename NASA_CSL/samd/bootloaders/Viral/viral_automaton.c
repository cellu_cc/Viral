#include "viral_automaton.h"
#include <stdlib.h>


extern SerialPort APA0;
extern SerialPort APA1;
extern SerialPort APA2;
extern SerialPort APA3;

extern SerialPort *prog_uart, *boot_uart;

/*
 * ****************************
 * ****************************
 * ***** STATIC FUNCTIONS *****
 * ***** STATIC FUNCTIONS *****
 * ***** STATIC FUNCTIONS *****
 * ****************************
 * ****************************
 */

static bool poll_neighbor_output_buffer(neighbor_state *_neighbor){
  if(_neighbor->outbufferlen > 0 && _neighbor->port->MODULE->USART.INTFLAG.bit.DRE){
    _neighbor->port->MODULE->USART.DATA.reg = (uint8_t)*(_neighbor->out_location);
    (_neighbor->outbufferlen)--;
    (_neighbor->out_location)++;
  }
  if(_neighbor->outbufferlen == 0){
    _neighbor->out_location = _neighbor->output_buffer;
    return true;
  }
  return false;
}

static bool poll_neighbor_input_buffer(neighbor_state *_neighbor){
  _neighbor->input_val = 0;
  if(_neighbor->port->MODULE->USART.INTFLAG.bit.RXC){
    _neighbor->input_val = (uint8_t) _neighbor->port->MODULE->USART.DATA.reg;
    if(_neighbor->input_val == '#' || _neighbor->input_val == '\n')
      return true; //sharp found, return true so we can parse command
    else{
      if (('0' <= _neighbor->input_val && _neighbor->input_val <= '9'))
        _neighbor->current_number = (_neighbor->current_number << 4) | (_neighbor->input_val - '0');
      else if (('A' <= _neighbor->input_val) && (_neighbor->input_val <= 'F'))
        _neighbor->current_number = (_neighbor->current_number << 4) | (_neighbor->input_val - 'A' + 0xa);
      else if (('a' <= _neighbor->input_val) && (_neighbor->input_val <= 'f'))
        _neighbor->current_number = (_neighbor->current_number << 4) | (_neighbor->input_val - 'a' + 0xa);
      else
        _neighbor->command = _neighbor->input_val;
    }
  }
  return false; //sharp not found, so we aren't ready to parse the command.
}

//
//make sure we send this last message before we go
//I hate polling someday this needs to be an interrupt
//but not yet. not yet.
//if you get this reference we should be frands.
//
static void send_message_then_switch(neighbor_state *neighbor, uint8_t *message, uint8_t length, viral_state transition_state){
  for(int i = 0; i < length; i++)
    neighbor->output_buffer[neighbor->outbufferlen+i] = message[i];
  neighbor->outbufferlen+=length;
  uint32_t idle_count = 0;
  while(neighbor->outbufferlen > 0 && idle_count < 10000){
    if(neighbor->port->MODULE->USART.INTFLAG.bit.DRE){
      idle_count = 0; //reset count once DRE is cleared
      neighbor->port->MODULE->USART.DATA.reg = (uint8_t)*(neighbor->out_location);
      (neighbor->outbufferlen)--;
      (neighbor->out_location)++;
    }
    idle_count++;
  }

  //If we aren't idle, then switch to the new state- we sent everything.
  if(idle_count < MAX_POLLING_TRIES)
    automaton_state = transition_state;
}

//creating the proper neighbor structs
void initialize_automaton(){
  self_version = automaton_software_version();
  neighborA = (neighbor_state*)init_neighbor(&APA0);
  neighborB = (neighbor_state*)init_neighbor(&APA1);
  neighborC = (neighbor_state*)init_neighbor(&APA2);
  neighborD = (neighbor_state*)init_neighbor(&APA3);
}

neighbor_state * init_neighbor(SerialPort *_port){
  neighbor_state *returnstate = (neighbor_state*)malloc(sizeof(neighbor_state));
  returnstate->port = _port;
  returnstate->output_buffer = (uint8_t *)malloc(sizeof(uint8_t)*BUFFER_SIZE);
  returnstate->out_location = returnstate->output_buffer;
  returnstate->outbufferlen = 0;
  returnstate->input_val = 0;
  returnstate->current_number = 0;
  returnstate->command = 0;
  returnstate->timeout = 0;
  returnstate->version = 0;
  return returnstate;
}

//
// This function reads and sends polling information between itself and its
// neighbors. It should boot up and send a ping to each of its neighbors, asking for a version.
// For each responded version:
  // IF the version val from the neighbor is < than the existing version, THEN
    //it should send a reset command to the neighbor
    //it should set its program state to transmitting
    //it should set boot_uart to the APA port from which it received the reset command
  // IF the version val from the neighbor is > than the stored version, THEN
    //it should stay in the polling state and await a reset command
  // IF the version val from all neighbors = the stored version val, THEN
    //it should enter the program, since its job is done.
//

// NB: change this from bool to an error code
bool automaton_polling_state(){
  self_version = automaton_software_version();
  bool running = true;
  //there must be a better way of doing this
  if(update_neighbor(neighborA))
    if(update_neighbor(neighborB))
      if(update_neighbor(neighborC))
        update_neighbor(neighborD);

  if(neighborA->version == self_version && \
     neighborB->version == self_version && \
     neighborC->version == self_version && \
     neighborD->version == self_version
    )
    automaton_state = APPLICATION;

  return running;
}

void automaton_waiting_state(){
  boot_uart = NULL;

  if(poll_neighbor_input_buffer(neighborA))
    boot_uart = neighborA->port;
  if(poll_neighbor_input_buffer(neighborB))
    boot_uart = neighborB->port;
  if(poll_neighbor_input_buffer(neighborC))
    boot_uart = neighborC->port;
  if(poll_neighbor_input_buffer(neighborD))
    boot_uart = neighborD->port;

  if(boot_uart != NULL){
    sam_ba_monitor_init(SAM_BA_INTERFACE_USART, boot_uart);
    automaton_state = RECEIVING;
  }

}

//
// This function handles one node programming the other. It assumes that the
// prog_uart pointer references the port of the target board.
// This function:
//   1. polls the target with 'N#' until it receives a '\r\n' response
//   2. calls the samba programmer (over in sam_ba_programmer.c)
// If it goes X amount of time without a response, it returns to POLLING mode by
// setting the
//

bool automaton_transmitting_state(){

  if(poll_neighbor_output_buffer(targetNeighbor)){
    if(targetNeighbor->timeout > 100000){
      targetNeighbor->output_buffer[targetNeighbor->outbufferlen++] = 'N';
      targetNeighbor->output_buffer[targetNeighbor->outbufferlen++] = '#';
      targetNeighbor->timeout=0;
    }
  }
  if(poll_neighbor_input_buffer(targetNeighbor)){
    switch(targetNeighbor->command){
      case '\r':
        send_message_then_switch(targetNeighbor,(uint8_t*)"N#",0,TRANSMITTING);
        sam_ba_program(prog_uart);
        automaton_state = POLLING;
        break;
    }
  }
  targetNeighbor->timeout++;
  return true;
}

bool automaton_receiving_state(){
  sam_ba_monitor_run();    /* SAM-BA on Serial loop */
}

bool update_neighbor(neighbor_state *neighbor){
  if(poll_neighbor_output_buffer(neighbor)){
    if((neighbor->timeout) > 100000 && neighbor->version < self_version){
      if(neighbor->version > 0)
        neighbor->output_buffer[neighbor->outbufferlen++] = 'r';
      else
        neighbor->output_buffer[neighbor->outbufferlen++] = 'v';

      neighbor->output_buffer[neighbor->outbufferlen++] = '#';
      neighbor->timeout = 0;
    }
  }
  if(poll_neighbor_input_buffer(neighbor)){
    if(neighbor->command == 'v'){
      if(neighbor->current_number == 0){
        neighbor->output_buffer[neighbor->outbufferlen++] = 'v';
        uint8_t buff[8];
        uint32_t tself_version = self_version;
        for(int i = 0; i < 8; i++){
          uint8_t d = tself_version & 0xF;
          tself_version = (tself_version >> 4);
          buff[7-i] = d > 9 ? 'A' + d - 10 : '0' + d;
        }
        for(int i = 0; i < 8; i++){
          neighbor->output_buffer[neighbor->outbufferlen++] = buff[i];
        }
        neighbor->output_buffer[neighbor->outbufferlen++] = '#';
      }
      else{
        neighbor->version = neighbor->current_number;
        if((self_version > neighbor->current_number || neighbor->current_number == 0xFFFFFFFF) && self_version != 0xFFFFFFFF){
          neighbor->output_buffer[neighbor->outbufferlen++] = 'r';
          neighbor->output_buffer[neighbor->outbufferlen++] = '#';
        }
      }
    }
    else if(neighbor->command == 'r'){
      boot_uart = neighbor->port;
      send_message_then_switch(neighbor, (uint8_t*)"k#", 2, WAITING_FOR_START);
    }
    else if(neighbor->command == 'k'){
      //acknowledgement of a 'program' command from the target
      //so enter program mode
      prog_uart = neighbor->port;
      targetNeighbor = neighbor;
      automaton_state = TRANSMITTING;
      return false;
      //sam_ba_monitor_init(SAM_BA_INTERFACE_USART, boot_uart);
    }
    neighbor->command = 'z';
    neighbor->current_number = 0;
    //neighbor->timeout = 0;
  }
  neighbor->timeout++;
  if(neighbor->timeout > MAX_POLLING_TRIES)
    neighbor->version = self_version;
  return true;
}


uint32_t automaton_software_version(){
  uint32_t returnval = 0;
  for(int i = 0; i < 4; i++){
    returnval = returnval << 8;
    returnval = (returnval|(vrow_data[i]&0xFF));
  }
  return returnval;
}
