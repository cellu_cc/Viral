/*
  Copyright (c) 2015 Arduino LLC.  All right reserved.
  Copyright (c) 2015 Atmel Corporation/Thibaut VIARD.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "status_codes.h"
#include <stdio.h>
#include <sam.h>
#include "sam_ba_monitor.h"
#include "board_definitions.h"
#include "sam_ba_serial.h"
#include "board_driver_led.h"
#include "sam_ba_usb.h"
#include "sam_ba_cdc.h"
#include "viral_automaton.h"
#include "sam_ba_programmer.h"

extern uint32_t __sketch_vectors_ptr; // Exported value from linker script
extern void board_init(void);

SerialPort APA0;
SerialPort APA1;
SerialPort APA2;
SerialPort APA3;

SerialPort *prog_uart;
SerialPort *boot_uart;

volatile viral_state automaton_state;

uint8_t vrow_data[VROW_SIZE];

#if (defined DEBUG) && (DEBUG == 1)
volatile uint32_t* pulSketch_Start_Address;
#endif

//static volatile bool serial_sharp_received_flag = false;

#if DEBUG_ENABLE
#	define DEBUG_PIN_HIGH 	port_pin_set_output_level(BOOT_LED, 1)
#	define DEBUG_PIN_LOW 	port_pin_set_output_level(BOOT_LED, 0)
#else
#	define DEBUG_PIN_HIGH 	do{}while(0)
#	define DEBUG_PIN_LOW 	do{}while(0)
#endif

/**
 * \brief Check the application startup condition
 *
 */
static void check_start_application(void)
{
  LED_init();
  LEDRX_init();
  LEDTX_init();
  LED_off();
  LEDRX_off();
  LEDTX_off();

#if defined(BOOT_DOUBLE_TAP_ADDRESS)
  #define DOUBLE_TAP_MAGIC 0x07738135
  if (PM->RCAUSE.bit.POR)
  {
    /* On power-on initialize double-tap */
    BOOT_DOUBLE_TAP_DATA = 0;
  }
  else
  {
    if ((BOOT_DOUBLE_TAP_DATA&0xFFFFFFF0) == 0x07738130){
      automaton_state = WAITING_FOR_START;
      switch(BOOT_DOUBLE_TAP_DATA&0x0000000F){
        case 0x0:
          boot_uart = &APA0;
          break;
        case 0x1:
          boot_uart = &APA1;
          break;
        case 0x2:
          boot_uart = &APA2;
          break;
        case 0x3:
          boot_uart = &APA3;
          break;
        case 0x5:
          boot_uart = &APA0;
          automaton_state = WAITING_FOR_START;
          break;
      }
    }

    if (BOOT_DOUBLE_TAP_DATA == DOUBLE_TAP_MAGIC)
    {
      /* Second tap, stay in bootloader */
      BOOT_DOUBLE_TAP_DATA = 0;
      automaton_state = WAITING_FOR_START;
      return;
    }

    /* First tap */
    BOOT_DOUBLE_TAP_DATA = DOUBLE_TAP_MAGIC;

    /* Wait 0.5sec to see if the user tap reset again.
     * The loop value is based on SAMD21 default 1MHz clock @ reset.
     */
    for (uint32_t i=0; i<125000; i++) /* 500ms */
      /* force compiler to not optimize this... */
      __asm__ __volatile__("");

    /* Timeout happened, continue boot... */
    BOOT_DOUBLE_TAP_DATA = 0;
  }
#endif

  return;
}

static void determine_reset(){
  #if (!defined DEBUG) || ((defined DEBUG) && (DEBUG == 0))
    uint32_t* pulSketch_Start_Address;
  #endif

    /*
     * Test sketch stack pointer @ &__sketch_vectors_ptr
     * Stay in SAM-BA if value @ (&__sketch_vectors_ptr) == 0xFFFFFFFF (Erased flash cell value)
     */

    if (__sketch_vectors_ptr == 0xFFFFFFFF)
    {
      /* Stay in bootloader */
      return;
    }

    /*
     * Load the sketch Reset Handler address
     * __sketch_vectors_ptr is exported from linker script and point on first 32b word of sketch vector table
     * First 32b word is sketch stack
     * Second 32b word is sketch entry point: Reset_Handler()
     */
    pulSketch_Start_Address = &__sketch_vectors_ptr ;
    pulSketch_Start_Address++;

    /*
     * Test vector table address of sketch @ &__sketch_vectors_ptr
     * Stay in SAM-BA if this function is not aligned enough, ie not valid
     */
    if ( ((uint32_t)(&__sketch_vectors_ptr) & ~SCB_VTOR_TBLOFF_Msk) != 0x00)
    {
      /* Stay in bootloader */
      return;
    }

  /*
  #if defined(BOOT_LOAD_PIN)
    volatile PortGroup *boot_port = (volatile PortGroup *)(&(PORT->Group[BOOT_LOAD_PIN / 32]));
    volatile bool boot_en;

    // Enable the input mode in Boot GPIO Pin
    boot_port->DIRCLR.reg = BOOT_PIN_MASK;
    boot_port->PINCFG[BOOT_LOAD_PIN & 0x1F].reg = PORT_PINCFG_INEN | PORT_PINCFG_PULLEN;
    boot_port->OUTSET.reg = BOOT_PIN_MASK;
    // Read the BOOT_LOAD_PIN status
    boot_en = (boot_port->IN.reg) & BOOT_PIN_MASK;

    // Check the bootloader enable condition
    if (!boot_en)
    {
      // Stay in bootloader
      return;
    }
  #endif
  */
    //uint8_t tmpOut[8];
    //volatile uint8_t outnum = snprintf((char *)tmpOut, sizeof(tmpOut), "%08X", &pulSketch_Start_Address);
    //serial_putdata(boot_uart, "helloooo", 8);

    LED_on();

    /* Rebase the Stack Pointer */
    __set_MSP( (uint32_t)(__sketch_vectors_ptr) );

    /* Rebase the vector table base address */
    SCB->VTOR = ((uint32_t)(&__sketch_vectors_ptr) & SCB_VTOR_TBLOFF_Msk);

    /* Jump to application Reset Handler in the application */
    asm("bx %0"::"r"(*pulSketch_Start_Address));
}


/**
 *  \brief SAMD21 SAM-BA Main loop.
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
  DEBUG_PIN_HIGH;
  automaton_state = POLLING;
  /* Jump in application if condition is satisfied */
  check_start_application();

  /* We have determined we should stay in the monitor. */
  /* System initialization */
  board_init();
  __enable_irq();

  /* UART is enabled in all cases */

  APA0 = SERIAL_PORT(0);
  APA1 = SERIAL_PORT(1);
  APA2 = SERIAL_PORT(2);
  APA3 = SERIAL_PORT(3);

  serial_open(&APA0);
  serial_open(&APA1);
  serial_open(&APA2);
  serial_open(&APA3);

  read_block_NVM_to_buffer((NVMDATA_ADDRESS-VROW_SIZE),vrow_data, VROW_SIZE);
  DEBUG_PIN_LOW;
  LED_off();
  LEDRX_off();
  LEDTX_off();

  initialize_automaton();

  while(1){
    if(automaton_state == POLLING){
      automaton_polling_state();
      pulse_led(GREEN, 1);
    }
    else if(automaton_state == WAITING_FOR_START){
      automaton_waiting_state();
      pulse_led(BLUE,3);
      pulse_led(RED,3);
    }
    else if(automaton_state == RECEIVING){
      automaton_receiving_state();
      pulse_led(RED, 3); // while we're waiting, blink the D13
    }
    else if(automaton_state == TRANSMITTING){
      automaton_transmitting_state();
      pulse_led(BLUE, 3);
    }
    else{
      determine_reset();
      pulse_led(RED,10);
    }
  }
}

static uint16_t pulse_tick[3] = {0,0,0};
#define BOOT_PULSE_MAX 1000
static int8_t  pulse_dir[3]={1,1,1};
static int16_t pulse_pwm[3]={0,0,0};
void pulse_led(board_led color, int8_t speed) {
  // blink D13
  pulse_tick[color]++;
  if (pulse_tick[color]==BOOT_PULSE_MAX) {
    pulse_tick[color] = 0;
    pulse_pwm[color] += pulse_dir[color] * speed;
    if (pulse_pwm[color] > 255) {
      pulse_pwm[color] = 255;
      pulse_dir[color] = -1;
    }
    if (pulse_pwm[color] < 0) {
      pulse_pwm[color] = 0;
      pulse_dir[color] = +1;
    }
    if(color == RED)
      LED_on();
    else if(color == BLUE)
      LEDRX_on();
    else
      LEDTX_on();
  }
  if (pulse_tick[color]==pulse_pwm[color]){
    if(color == RED)
      LED_off();
    else if(color == BLUE)
      LEDRX_off();
    else
      LEDTX_off();
  }
}
