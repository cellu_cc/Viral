#include "sam_ba_programmer.h"
#include "delay.h"
#include <stdio.h>
#include <assert.h>


//Starting location of the program memory, from the linker script
extern uint32_t __sketch_vectors_ptr;



static uint8_t hexVal(uint8_t input){
  if(input < 9)
    return input+48;
  else
    return input+55;
}

static uint32_t decVal(uint8_t *cmd){
  uint32_t retval = 0;
  for(int i = 0; i < 8; i++){
    if(cmd[i] > 64)
      retval |= (((cmd[i]-55) & 0xF) << ((7-i)*4));
    else
      retval |= (((cmd[i]-48) & 0xF) << ((7-i)*4));
  }
  return retval;
}

//
// checks if the host NVM controller is ready
//
static inline bool hostNVMIsReady(void)
{
	/* Get a pointer to the module hardware instance */
	Nvmctrl *const nvm_module = NVMCTRL;

	return nvm_module->INTFLAG.reg & NVMCTRL_INTFLAG_READY;
}

//
// checks if the target NVM controller is ready
//
static bool targetNVMIsReady(SerialPort *serPort){
  uint8_t int_flag = programmer_read_byte(serPort, NVM_INT_STATUS_REG) & NVM_INT_STATUS_READY_MASK;
  return int_flag == 1;
}

//
// Pads the writeWord method with two isReady calls to make sure the NVM
// controller is prepared
//
static void executeNVMCommand(SerialPort *serPort, uint32_t cmd){
  //wait for the NVM to prepare itself
  while(!targetNVMIsReady(serPort)){
  }
  programmer_write_word(serPort, NVM_CTRLA_REG, cmd);
  //wait for the NVM to recover
  while(!targetNVMIsReady(serPort)){
  }
}

void sam_ba_program(SerialPort *serPort){
    //verify that the board can talk and is what we think it is
    programmer_verify_board(serPort);

    //Erase Flash
    programmer_erase_target(serPort);

    // temp array holding the flash
    uint8_t tbuf[FLASH_SIZE];

    //address of the start of the sketch data
    uint32_t basevector = (uint32_t)&__sketch_vectors_ptr;
    //temp variable
    uint32_t pagenum;

    //Calculate program size from NVM
    uint32_t progsize = ((uint32_t *)vrow_data)[1];

    //Calculate the number of chunks that need to be sent
    progsize = (progsize/FLASH_SIZE)+1;
    //Iterate through the chunks
    for(pagenum = 0; pagenum < progsize; pagenum++){
      //turn the LED on
      //largely for debug (looks cool)
      LEDRX_on();
      //
      programmer_write(serPort, basevector+pagenum*FLASH_SIZE, FLASH_SIZE);
      //serial_putdata(boot_uart, "success", 7);
      LEDRX_off();
      programmer_write_buffer(serPort, BUFFER_ADDRESS, basevector+pagenum*FLASH_SIZE, FLASH_SIZE);
#ifdef VIRAL_DEBUG
      tbuf[0] = (pagenum&0xFF);
      serial_putdata(boot_uart, tbuf, 1);
#endif
    }
    for(pagenum = 0; pagenum < progsize; pagenum++){
      uint16_t targetcrc = programmer_checksum_target(serPort, basevector+FLASH_SIZE, FLASH_SIZE);
      uint16_t selfcrc = programmer_checksum_self(basevector+FLASH_SIZE, FLASH_SIZE);
      //assert(targetcrc == selfcrc);
#ifdef VIRAL_DEBUG
      if(targetcrc != selfcrc){
        cmdLen = snprintf((char*)cmdBuffer, sizeof(cmdBuffer), "crcerror: %04X!=%04X", (unsigned int) targetcrc, (unsigned int)selfcrc);
        serial_putdata(boot_uart, cmdBuffer, cmdLen);
      }
      else{
        serial_putdata(boot_uart, "s", 1);
      }
#endif
    }

    programmer_update_vrow(serPort);

    cmdBuffer[0] = 'Q';
    cmdBuffer[1] = '#';
    serial_putdata(serPort, cmdBuffer, 2);
    //Write X pages, starting at 0x2000 on the source board, to 0x20005000 on destination, in increments of 0x1000
      // From BOSSA:
        // from commandMWF::invoke in Command.cpp
          // opens the infile
          // writes 1024 bytes to a buffer
          // calls samba.write
        // from samba.write
          // sends S[ADDR],[SIZE]# to the target
          // calls writeXmodem with the buffer and size
        // from samba.writeXmodem
          //checks for the start response from the target
          //loops through the buffer in increments of 128 bytes
            //sets the first 3 characters of the xmodem block to be
              // an SOH char
              // block number byte
              // inverse block number byte
            //moves either 128 bytes or the remaining number of bytes to the buffer
            //calls CRC16Add for this block
            //tries to write the block over serial
              //checks for an ACK after each write
          //tries to send an EOT character
            //checks if it received an ACK
}


void programmer_set_normal_mode(SerialPort *serPort){
  //Send 'N#'
  //Read in 2 bytes '\n\r'
  cmdBuffer[0] = 'N';
  cmdBuffer[1] = '#';
  serial_putdata(serPort, cmdBuffer, 2);
  programmer_get_data_and_wait(serPort, cmdBuffer, 2);
}

void programmer_get_data_and_wait(SerialPort *serPort, uint8_t *buf, uint32_t length){
  uint32_t total = 0;
  while(total < length){
    total += serial_getdata(serPort, buf+total, length);
  }
}

bool programmer_verify_board(SerialPort *serPort){
  //ReadWord at 0x0
  return (programmer_read_word(serPort, 0x0) == 0x20007ffc);
  //ReadWord at 0xe000ed00
  //programmer_read_word(serPort, buf, 0xe000ed00);
  //ReadWord at 0x41002018
  //return programmer_read_word(serPort, 0x41002018);
  //return programmer_read_word(serPort, 0x18200041);//41002018);
}

//
// Update the target's software version and code size to match
// the host's.
//
void programmer_update_vrow(SerialPort *serPort){
  uint32_t *tvrowdata = (uint32_t *)vrow_data;

  cmdLen = snprintf((char*) cmdBuffer, sizeof(cmdBuffer), "I0,%08X#",(unsigned int)tvrowdata[0]);
  programmer_write_with_polling(serPort, cmdBuffer, (uint8_t)cmdLen, 3, TIMEOUT_LONG);
  assert(cmdBuffer[cmdLen]=='I');

  cmdLen = snprintf((char*) cmdBuffer, sizeof(cmdBuffer), "I4,%08X#",(unsigned int)tvrowdata[1]);
  programmer_write_with_polling(serPort, cmdBuffer, (uint8_t)cmdLen, 3, TIMEOUT_LONG);
  assert(cmdBuffer[cmdLen]=='I');

  //commit the buffer to non-volatile memory
  cmdBuffer[0] = 'i';
  cmdBuffer[1] = '#';
  serial_putdata(serPort, cmdBuffer, 2);
  cmdBuffer[0] = 0;
  programmer_get_data_and_wait(serPort, cmdBuffer, 3);
}

//
// TODO: Add exception checks
//
uint16_t programmer_checksum_target(SerialPort *serPort, uint32_t start_address, uint32_t size){
  cmdLen = snprintf((char*)cmdBuffer, sizeof(cmdBuffer), "Z%08X,%08X#", (unsigned int) start_address, (unsigned int)size);
  programmer_write_with_polling(serPort, cmdBuffer, (uint8_t)cmdLen, 12, TIMEOUT_LONG);
  assert(cmdBuffer[cmdLen]=='Z');

  //unsigned int res;
  //sscanf((const char *)(cmdBuffer+1), "%x", &res);
  return decVal(cmdBuffer+1);
}

uint16_t programmer_checksum_self(uint32_t start_address, uint32_t size){
  uint8_t *data = (uint8_t *)start_address;
  uint16_t crc = 0;
  uint32_t i = 0;
  for(i = 0; i<size; i++)
    crc = serial_add_crc(*data++,crc);
  return crc;
}

void programmer_write_word(SerialPort *serPort, uint32_t addr, uint32_t val){
  cmdLen = snprintf((char*)cmdBuffer, sizeof(cmdBuffer), "W%08X,%08X#", (unsigned int)addr, (unsigned int)val);
  serial_putdata(serPort, cmdBuffer, cmdLen);
}

void programmer_reset_target(SerialPort *serPort){
  cmdBuffer[0] = 'Q';
  cmdBuffer[1] = '#';
  serial_putdata(serPort, cmdBuffer, 2);
}

void programmer_write(SerialPort *serPort, uint32_t src_addr, uint32_t size){
  read_block_NVM_to_buffer(src_addr, dataBuffer, size);
  cmdLen = snprintf((char*)cmdBuffer, sizeof(cmdBuffer), "S%08X,%08X#", BUFFER_ADDRESS, (unsigned int)size);
  //delayMicroseconds(10000);
  //programmer_write_with_polling(serPort, cmdBuffer, (uint8_t)cmdLen, 0, TIMEOUT_SHORT);
  //assert(cmdBuffer[cmdLen]==START);
  serial_putdata(serPort, cmdBuffer, cmdLen);
  programmer_get_data_and_wait(serPort, cmdBuffer, 1);
  serial_putdata_xmd(serPort, dataBuffer, size);
}

void programmer_write_buffer(SerialPort *serPort, uint32_t src_addr, uint32_t dst_addr, uint32_t size){
  //
  // First tell the target board the source address
  //
  cmdLen = snprintf((char*) cmdBuffer, sizeof(cmdBuffer), "Y%08X,0#",(unsigned int)src_addr);
  programmer_write_with_polling(serPort, cmdBuffer, (uint8_t)cmdLen, 3, TIMEOUT_LONG);
  assert(cmdBuffer[cmdLen]=='Y');
  //serial_putdata(serPort, cmdBuffer, cmdLen);
  //cmdBuffer[0] = 0;
  //programmer_get_data_and_wait(serPort, cmdBuffer, 3);
#ifdef VIRAL_DEBUG
  if(cmdBuffer[0]!='Y'){
    cmdBuffer[3] = 'f';
    serial_putdata(boot_uart, cmdBuffer, 4);
  }
#endif

  //
  // Then tell the target where to send data and how much
  //
  cmdLen = snprintf((char*) cmdBuffer, sizeof(cmdBuffer), "Y%08X,%08X#",(unsigned int)dst_addr, (unsigned int)size);
  programmer_write_with_polling(serPort, cmdBuffer, (uint8_t)cmdLen, 3, TIMEOUT_LONG);
  assert(cmdBuffer[cmdLen]=='Y');
  //serial_putdata(serPort, cmdBuffer, cmdLen);

#ifdef VIRAL_DEBUG
  serial_putdata(boot_uart, cmdBuffer, cmdLen);
#endif

  //cmdBuffer[0] = 0;
  //programmer_get_data_and_wait(serPort, cmdBuffer, 3);

#ifdef VIRAL_DEBUG
  if(cmdBuffer[0]!='Y'){
    cmdBuffer[1] = 'F';
    serial_putdata(boot_uart, cmdBuffer, 2);
  }
#endif

}

uint32_t programmer_read_word(SerialPort *serPort, uint32_t addr){
  cmdLen = snprintf((char*)cmdBuffer, sizeof(cmdBuffer), "w%08X,4#", (unsigned int)addr);
  serial_putdata(serPort, cmdBuffer, cmdLen);
  programmer_get_data_and_wait(serPort, cmdBuffer, 4);

  uint32_t value = (cmdBuffer[3] << 24 |
                    cmdBuffer[2] << 16 |
                    cmdBuffer[1] <<  8 |
                    cmdBuffer[0] <<  0 );
  return value;
}

uint8_t programmer_read_byte(SerialPort *serPort, uint32_t addr){
  cmdLen = snprintf((char*)cmdBuffer, sizeof(cmdBuffer), "o%08X,4#", (unsigned int)addr);
  serial_putdata(serPort, cmdBuffer, cmdLen);
  programmer_get_data_and_wait(serPort, cmdBuffer, 1);
  return (uint8_t)cmdBuffer[0];
}

void programmer_erase_target(SerialPort *serPort){
  //For SAMD21- eraseAll is inside NVMFlash.cpp in BOSSA
  // Calculate starting row given bootloader size
  // Calculate total # of rows given memory size
    // subtract EEPROM SPACE from the total
  uint32_t starting_row = BOOTLOADERPAGES / FLASHPAGESPERROW;
  uint32_t total_rows = (NVMPAGES - EEPROMPAGES)/FLASHPAGESPERROW;
  //loop through each row of flash
  for(uint32_t row = starting_row; row < total_rows; row++){
    // calculate the address of each row
    uint32_t addr_in_flash = (row * FLASHPAGESPERROW * BYTESPERPAGE);
    addr_in_flash = addr_in_flash / 2;
    // wait for the NVM ctrl to be ready
    //This might not be necessary every time
    while(!targetNVMIsReady(serPort)){
    }
    // Clear the error bits
      // read the status register
    uint16_t status_reg = programmer_read_word(serPort, NVM_STATUS_REG) & 0xffff;
      // write the status register + MASK
    programmer_write_word(serPort, NVM_STATUS_REG, status_reg | NVMCTRL_STATUS_MASK);
    // issue the erase command
    //Write the desired address to erase to the NVM controller
    programmer_write_word(serPort, NVM_ADDR_REG, addr_in_flash);
    //send the command to erase the row
    executeNVMCommand(serPort, CMD_ERASE_ROW);

    //dbg_arr[0] = (row>>8)&0xff;
    //dbg_arr[1] = row&0xff;
    //serial_putdata(boot_uart, dbg_arr, 2);
  }

}

//
// Write the input buffer to the port and poll for a response
// rewrite to the input buffer if no response.
// Repeat 3 times, if no luck, return false
// Returns whether the function was able to write
//
bool programmer_write_with_polling(SerialPort *serPort, uint8_t *buf, uint8_t send_length, uint8_t response_length, uint32_t timeout){
  uint8_t total = 0;
  uint32_t ttimeout = timeout;
  while(total<response_length){
    if((ttimeout--)==0){
      serial_putdata(serPort, buf, send_length);
      total = 0;
      ttimeout = timeout;
    }
    if(serial_is_rx_ready(serPort))
      total += serial_getdata(serPort, buf+send_length+total, response_length);
  }
  return true;
}








bool read_block_NVM_to_buffer(const uint32_t source_address, uint8_t * tbuffer, uint32_t size){

  if (source_address & (FLASH_PAGE_SIZE - 1)) {
		return false;
	}
  Nvmctrl *const nvm_module = NVMCTRL;

  /* Check if the module is busy */
  while (!hostNVMIsReady()) {
  }

  /* Clear error flags */
  nvm_module->STATUS.reg = NVMCTRL_STATUS_MASK;

	uint32_t page_address = source_address / 2;

  /* NVM _must_ be accessed as a series of 16-bit words, perform manual copy
   * to ensure alignment */
  for (uint16_t i = 0; i < size; i += 2) {
    /* Fetch next 16-bit chunk from the NVM memory space */
    uint16_t data = NVM_MEMORY[page_address++];

    /* Copy first byte of the 16-bit chunk to the destination buffer */
    tbuffer[i] = (data & 0xFF);

    /* If we are not at the end of a read request with an odd byte count,
     * store the next byte of data as well */
    if (i < (size - 1)) {
      tbuffer[i + 1] = (data >> 8);
    }
  }
  return true;
}
